from django.apps import AppConfig


class VidaMarinaAppConfig(AppConfig):
    name = 'vida_marina_app'
