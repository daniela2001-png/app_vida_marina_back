from django.contrib import admin

from .models.user import User
from .models.animales import Animales
from .models.tipo_estado_animal import EstadoAnimal
from .models.aventuras import Aventuras
from .models.tipo_estado_aventura import EstadoAventura
from .models.tipo_estado_user import EstadoUsuario
from .models.tipo_rol import TipoRol
from .models.animales_vistos import AnimalesVistos

# Register your models here.
admin.site.register(User)
admin.site.register(Animales)
admin.site.register(AnimalesVistos)
admin.site.register(EstadoAnimal)
admin.site.register(EstadoAventura)
admin.site.register(EstadoUsuario)
admin.site.register(TipoRol)
admin.site.register(Aventuras)
