import json
from django.http.response import JsonResponse
from vida_marina_app.models.animales import Animales
from vida_marina_app.serializers.animales import AnimalesSerializer
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer


def return_animals(request):
    content = dict()
    queryset = Animales.objects.all()
    lista = []
    for item in queryset:
        fic_item = AnimalesSerializer(item)
        lista.append(fic_item.data)
    content["data"] = lista
    json_data = JSONRenderer().render(lista)
    return HttpResponse(json_data)


def return_animal_by_id(request, id):
    queryset = Animales.objects.all()
    lista = []
    for item in queryset:
        fic_item = AnimalesSerializer(item)
        lista.append(fic_item.data)
    for item in lista:
        if item["id"] == id:
            print(item)
            json_data = JSONRenderer().render(item)
    return HttpResponse(json_data)
