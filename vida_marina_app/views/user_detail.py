from django.conf import settings
from rest_framework_simplejwt.backends import TokenBackend
import json
from rest_framework import generics
from django.http.response import HttpResponse
from rest_framework.renderers import JSONRenderer
from vida_marina_app.models.user import User
from vida_marina_app.serializers.user import UserSerializer


class UserDetailView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        try:
            token_str = kwargs["token"]
            tokenBackend = TokenBackend(
                algorithm=settings.SIMPLE_JWT['ALGORITHM'])
            valid_data = tokenBackend.decode(token_str, verify=False)
            user = User.objects.get(id=valid_data['user_id'])
            user_object = self.serializer_class(user)
            json_data = JSONRenderer().render(user_object.data)
            return HttpResponse(json_data)
        except Exception:
            return HttpResponse(json.dumps({"error": "Invalid or expired token"}))
