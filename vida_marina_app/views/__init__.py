from .crear_user import UserCreateView
from .user_detail import UserDetailView
from .animales import return_animals, return_animal_by_id
from .show_score_user import response_user
