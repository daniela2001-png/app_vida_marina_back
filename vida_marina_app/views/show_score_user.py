import json
from django.http.response import JsonResponse
from vida_marina_app.models.aventuras import Aventuras
from vida_marina_app.serializers.aventuras import AventurasSerializer
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer


def response_user(request):
    aventuras = Aventuras.objects.all()
    lista = []
    content = {}
    for item in aventuras:
        fic_item = AventurasSerializer(item)
        lista.append(fic_item.data)
    content["data"] = lista
    json_data = JSONRenderer().render(lista)
    return HttpResponse(json_data)
