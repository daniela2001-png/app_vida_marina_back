from vida_marina_app.models.tipo_estado_animal import EstadoAnimal
from rest_framework import serializers


class EstadoAnimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstadoAnimal
        fields = "__all__"
