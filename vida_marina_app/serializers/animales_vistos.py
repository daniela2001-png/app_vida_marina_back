from vida_marina_app.models.animales_vistos import AnimalesVistos
from rest_framework import serializers
from .animales import AnimalesSerializer
from .user import UserSerializer


class AnimalesVistosSerializer(serializers.ModelSerializer):
    animal = AnimalesSerializer
    usuario = UserSerializer

    class Meta:
        model = AnimalesVistos
        fields = "__all__"
