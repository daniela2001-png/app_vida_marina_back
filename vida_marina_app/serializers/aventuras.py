from vida_marina_app.models.aventuras import Aventuras
from rest_framework import serializers
from .user import UserSerializer
from .estado_aventura import EstadoAventuraSerializer


class AventurasSerializer(serializers.ModelSerializer):
    user = UserSerializer
    estado_aventura = EstadoAventuraSerializer

    class Meta:
        model = Aventuras
        fields = "__all__"
