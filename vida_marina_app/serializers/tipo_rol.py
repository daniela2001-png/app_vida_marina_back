from vida_marina_app.models.tipo_rol import TipoRol
from rest_framework import serializers


class RolSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoRol
        fields = "__all__"
