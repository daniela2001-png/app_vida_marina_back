from vida_marina_app.models.tipo_estado_user import EstadoUsuario
from rest_framework import serializers


class EstadoUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstadoUsuario
        fields = "__all__"
