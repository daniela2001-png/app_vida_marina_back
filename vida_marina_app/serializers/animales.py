from vida_marina_app.models.animales import Animales
from rest_framework import serializers
from .estado_animal import EstadoAnimalSerializer


class AnimalesSerializer(serializers.ModelSerializer):
    estado_animal = EstadoAnimalSerializer

    class Meta:
        model = Animales
        fields = "__all__"
