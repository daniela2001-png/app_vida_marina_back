from vida_marina_app.models.user import User
from rest_framework import serializers
from .tipo_rol import RolSerializer
from .estado_user import EstadoUserSerializer
from vida_marina_app.models.tipo_rol import TipoRol
from vida_marina_app.models.tipo_estado_user import EstadoUsuario


class UserSerializer(serializers.ModelSerializer):
    tipo_rol = RolSerializer
    estado_user = EstadoUserSerializer

    class Meta:
        model = User
        fields = ["id_usuario", "rol", "username", "password", "nombres", "apellidos", "id_estado_usuario"]
