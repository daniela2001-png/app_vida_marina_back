from .animales import AnimalesSerializer
from .animales_vistos import AnimalesVistosSerializer
from .aventuras import AventurasSerializer
from .estado_animal import EstadoAnimalSerializer
from .estado_aventura import EstadoAventuraSerializer
from .estado_user import EstadoUserSerializer
from .tipo_rol import RolSerializer
from .user import UserSerializer
