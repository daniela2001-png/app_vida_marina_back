from vida_marina_app.models.tipo_estado_aventura import EstadoAventura
from rest_framework import serializers


class EstadoAventuraSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstadoAventura
        fields = "__all__"
