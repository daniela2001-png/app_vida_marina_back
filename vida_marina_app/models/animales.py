from django.db import models
from .tipo_estado_animal import EstadoAnimal


class Animales(models.Model):
    id = models.BigAutoField(primary_key=True)
    estado_animal_fk = models.ForeignKey(
        EstadoAnimal, related_name="id_estado_animal", on_delete=models.CASCADE)
    nombre = models.CharField('nombre', max_length=256)
    descripcion = models.CharField('descripcion', max_length=300)
    puntaje = models.CharField('puntaje', max_length=250)
