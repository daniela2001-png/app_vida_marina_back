from django.db import models


class TipoRol(models.Model):
    id_rol = models.BigAutoField(primary_key=True)
    nombre_rol = models.CharField('nombre_rol', max_length=20)
