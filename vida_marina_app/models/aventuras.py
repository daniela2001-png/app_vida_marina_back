from django.db import models
from .user import User
from .tipo_estado_aventura import EstadoAventura


class Aventuras(models.Model):
    id_aventura = models.BigAutoField(primary_key=True)
    usuario = models.ForeignKey(
        User, on_delete=models.CASCADE)
    estado_aventura = models.ForeignKey(
        EstadoAventura, on_delete=models.CASCADE)
    puntaje = models.CharField('puntaje', max_length=256)
