from django.db import models


class EstadoAnimal(models.Model):
    id_estado_animal_id = models.BigAutoField(primary_key=True)
    estado_animal = models.CharField('estado_animal', max_length=20)
