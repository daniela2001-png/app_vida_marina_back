from django.db import models

from .user import User
from .animales import Animales


class AnimalesVistos(models.Model):
    id_animal_visto = models.BigAutoField(primary_key=True)
    usuario = models.ForeignKey(
        User, on_delete=models.CASCADE)
    animal = models.ForeignKey(
        Animales, on_delete=models.CASCADE)
