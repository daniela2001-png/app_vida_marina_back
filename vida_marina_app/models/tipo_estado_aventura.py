from django.db import models


class EstadoAventura(models.Model):
    id_estado_aventura = models.BigAutoField(primary_key=True) 
    estado_aventura = models.CharField('estado_aventura', max_length=20)
