from django.db import models


class EstadoUsuario(models.Model):
    id_estado_usuario = models.BigAutoField(primary_key=True)
    estado_usuario = models.CharField('estado_usuario', max_length=20)
